const extract = require('extract-text-webpack-plugin');

module.exports = function() {
    return {
        module: {
            rules: [{
                    test: /\.(sass|scss)$/,
                    use: extract.extract({
                        use: [{
                                loader: 'css-loader',
                                options: {
                                    sourceMap: true,
                                    minimize: true
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true,
                                    minimize: true
                                }
                            }
                        ],
                        fallback: 'style-loader',
                    }),
                },
                {
                    test: /\.css$/,
                    use: extract.extract({
                        fallback: 'style-loader',
                        use: [{
                                loader: 'css-loader'
                            }
                        ],
                    }),
                }
            ]
        },
        plugins: [
            new extract({
                filename: './static/css/main.css'
            })
        ]
    };
};
