module.exports = function() {
    return {
        module: {
            rules: [{
                test: /\.css$/,
                use: [{
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            localIdentName: '[local]'
                        }
                    },
                    // {
                    //     loader: 'group-css-media-queries-loader'
                    // }
                ],
            }]
        }
    };
};
