module.exports = function() {
    return {
        module: {
            rules: [{
                test: /\.json$/,
                use: [{
                    loader: 'json-loader'
                }]
            }]
        }
    };
};
