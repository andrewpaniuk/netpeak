const initialState = 'N/A';

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SET_TITLE':
            return action.payload;
        default:
            return state;
    }
}
