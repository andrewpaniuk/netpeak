import { combineReducers } from 'redux';

import title from './title';
import search from './search';

export default combineReducers({
    title,
    search
});
