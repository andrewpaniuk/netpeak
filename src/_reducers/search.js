const initialState = '';

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SEARCH_POST':
            return action.payload;
        default:
            return state;
    }
}
