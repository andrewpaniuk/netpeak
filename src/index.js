import 'font-awesome/scss/font-awesome.scss';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './_reducers';

import React from 'react';
import ReactDOM from 'react-dom';
import './styles.sass';
import AppComponent from './components/app.component';

const store = createStore(reducer);

ReactDOM.render( <Provider store={store}><AppComponent/></Provider>, document.getElementById('root'));
