import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './layout/layout';
import Blog from './blog/blog';
import About from './about/about';
import BlogDetail from './blog-detail/blog-detail';
import P404 from './p404/p404';

class Router extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route exact path='/' component={Blog}/>
                    <Route path='/about' component={About}/>
                    <Route path='/page/:page' component={Blog}/>
                    <Route path='/post/:id' component={BlogDetail}/>
                    <Route path='*' component={P404}/>
                </Switch>
            </Layout>
        );
    }
}

export default Router;
