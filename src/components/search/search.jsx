import React, { Component } from 'react';
import { connect } from 'react-redux';

class Search extends Component {

    onChangeSearch = (e) => {
        this.props.onSearchPost(e.target.value);
    }

    render() {
        return (
            <div>
                <input type="text" onChange={this.onChangeSearch} placeholder='Search...'/>
            </div>
        );
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        onSearchPost: (search) => {
            dispatch({
                type: 'SEARCH_POST',
                payload: search
            })
        }
    })
)(Search);
