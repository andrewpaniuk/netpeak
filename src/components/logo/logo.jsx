import React, { Component } from 'react';
import './logo.sass';
import { Link } from 'react-router-dom';
import logo from './logo.png';

class Logo extends Component {
    render() {
        return (
            <Link to="/" className="logo"><img src={logo} alt="logo" /></Link>
        );
    }
}

export default Logo;
