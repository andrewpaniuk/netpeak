import React, { Component } from 'react';
import './app.component.sass';

import Router from './router';

import { BrowserRouter } from 'react-router-dom';

class AppComponent extends Component {

    render() {
        return (
            <BrowserRouter>
                <Router/>
            </BrowserRouter>
        )
    }
}

export default AppComponent;

