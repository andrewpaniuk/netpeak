import React, { Component } from 'react';
import './navigation.sass';
import { NavLink } from 'react-router-dom';

class Navigation extends Component {
    render() {
        return (
            <nav className="menu">
                <ul>
                    <li><NavLink to='/' exact activeClassName='active'>Home</NavLink></li>
                    <li><NavLink to='/about' activeClassName='active'>About</NavLink></li>
                </ul>
            </nav>
        );
    }
}

export default Navigation;
