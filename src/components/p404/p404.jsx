import React, { Component } from 'react';
import './p404.sass';

import { connect }from 'react-redux';
import { Link } from 'react-router-dom';

class P404 extends Component {

    componentWillMount = () => {
        this.props.onSetTitle('Page 404');
    }

    render() {
        return (
            <main className="page-404">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 col-xs-12">
                            <span className="header-middle-title">404</span>
                        </div>
                        <div className="col-md-12 col-xs-12">
                            <span className="header-middle-title2">page not found</span>
                        </div>
                        <div className="col-md-offset-4 col-md-4 col-xs-12">
                            <div className="header-info">
                                <p>THE LINK YOU FOLLOWED IS<br /> PROBABLY BROKEN OR<br /> THE PAGE HAS BEEN REMOVED</p>
                            </div>
                        </div>
                        <div className="col-md-offset-5 col-md-2 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8">
                            <button onClick={this.props.history.goBack} className="btn-read-more">Back</button>
                            <Link to='/' className="btn-discount">home</Link>
                        </div>
                        <div className="multi-bg"></div>
                    </div>
                </div>
            </main>
        );
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        onSetTitle: (title) => {
            dispatch({
                type: 'SET_TITLE',
                payload: title
            })
        }
    })
)(P404);
