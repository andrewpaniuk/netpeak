import React, { Component } from 'react';
import './modal.sass';

class Modal extends Component {

    render() {
        if (this.props.show) {
            document.body.classList.add('body-overflow');
        } else {
            document.body.classList.remove('body-overflow');
        }
        const showHideClassName = this.props.show ? "my-modal display-block" : "my-modal display-none";
        return (
            <div>
                <div className={showHideClassName}>
                    <section className="my-modal-main">
                        {this.props.children}
                        <div className="my-modal-close">
                            <button className='btn btn-danger' onClick={this.props.handleClose}>Close modal</button>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

export default Modal;
