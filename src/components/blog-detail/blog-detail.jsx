import React, { Component } from 'react';
import './blog-detail.sass';

import { connect } from 'react-redux';
import Loading from '../loading/loading';

class BlogDetail extends Component {

    state = {
        post: {},
        user: {},
        comments: [],
        loading: true
    }

    componentWillMount = async () => {
        try {
            const responsePost = await fetch(`https://jsonplaceholder.typicode.com/posts/${this.props.match.params.id}`);
            const post = await responsePost.json();
            const responseUser = await fetch(`https://jsonplaceholder.typicode.com/users/${post.userId}`);
            const user = await responseUser.json();
            const responseComments = await fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}/comments`);
            const comments = await responseComments.json();

            const localComments = JSON.parse(localStorage.getItem('comments')) || [];
            const filteredComments = localComments.filter(comment => comment.postId == post.id).reverse();
            comments.unshift(...filteredComments);
            this.setState({
                post,
                user,
                comments,
                loading: false
            })

        } catch (error) {
            console.log(error)
        }

        this.props.onSetTitle(this.state.post.title || 'No such POST');

    }

    showComments = () => {
        const { comments } = this.state;

        if (comments.length) {
            return comments.map(comment => {
                return (
                    <div key={comment.id} className="comment">
                        <div className="image-user">
                            <img src="img/user.png" alt="" />
                        </div>
                        <div className="comment-content">
                            <div className="name-date">
                                <div className="name-content">
                                    <span className="name">{comment.name}</span>
                                </div>
                                <span className="name-user">{comment.email}</span>
                            </div>
                            <div className="comment-desc">
                                {comment.body}
                            </div>
                        </div>
                    </div>
                );
            })
        }
        return null;

    }

    onSubmit = (e) => {
        e.preventDefault();
        const data = {
            name: this.comment_name.value,
            body: this.comment_body.value,
            postId: this.state.post.id,
            id: new Date().getTime().toString(),
            email: this.comment_email.value
        }

        let localItems = JSON.parse(localStorage.getItem('comments')) || [];
        localItems.push(data);

        localStorage.setItem('comments', JSON.stringify(localItems)) || [];

        const comments = this.state.comments;
        comments.unshift(data);
        this.setState({
            comments
        })
        this.comment_name.value = '';
        this.comment_body.value = '';
        this.comment_email.value = '';
    }

    showPost = () => {
        const { post, comments, user } = this.state;
        if (post.id) {
            return (
                <div className="post-content">
                    <div className="post">
                        <div className="row">
                            <div className="col-md-2">
                                <div className="left-info">
                                    <div className="comments">
                                        <div href="#"><span className="number-comment">{comments.length}</span>comments</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-10 line-dashed">
                                <div className="circle"></div>
                                <div className="right-blog">
                                    <div className="posted">
                                        <p>posted by <a href="#">{user.name}</a></p>
                                    </div>
                                    <div className="name-of-blog">
                                        <span>{post.title}</span>
                                    </div>
                                    <div className="description-of-blog">
                                        {post.body}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="comments-form">
                        <div className="row">
                            <div className="col-md-offset-2 col-md-10">
                                <div className="comments-content">
                                    <header>
                                        <h3>{comments.length ? 'Comments' : 'No comments yet!'}</h3>
                                    </header>
                                    {this.showComments()}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="reply">
                        <div className="row">
                            <div className="col-md-offset-2 col-md-10">
                                <div className="reply-form">
                                    <header>
                                        <h3>Leave a reply</h3>
                                        <p>Your email address will not be published. Required fields are marked<sup>&#10033;</sup></p>
                                    </header>
                                    <div className="reply-content">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <label htmlFor="name" name='name'>Title<sup>&#10033;</sup></label>
                                                <input type="text" id="name" ref={(input) => this.comment_name = input} required/>
                                                <label htmlFor="email">Email<sup>&#10033;</sup></label>
                                                <input type="email" id="email" name='email' ref={(input) => this.comment_email = input} required/>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="message-area">
                                                    <label htmlFor="body">Comments<sup>&#10033;</sup></label>
                                                    <textarea name="body" id="body" name='body' ref={(input) => this.comment_body = input} required></textarea>
                                                    <button onClick={this.onSubmit} className="btn-read-more">post comments</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <h2>No such post</h2>
        )
    }

    render() {
        return (
            <main className="single">
                <div className="container">
                    {!this.state.loading ? this.showPost() : <Loading size='big' center={true} />}
                </div>
            </main>
        );
    }
}

export default connect(
    state => ({
    }),
    dispatch => ({
        onSetTitle: (title) => {
            dispatch({
                type: 'SET_TITLE',
                payload: title
            })
        }
    })
)(BlogDetail);
