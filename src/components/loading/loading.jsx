import React, { Component } from 'react';
import './loading.sass';
import loading from './loading.gif';

class Loading extends Component {

    setStyleImg = () => {
        let style = {};
        switch (this.props.size) {
            case 'big':
                style = {
                    width: '100px',
                    height: '100px'
                }
                break;
            case 'small':
                style = {
                    width: '25px',
                    height: '25px'
                }
                break;

            default:
            style = {}
                break;
        }

        return style;
    }

    setCenter = () => {
        let style = {};
        if (this.props.center) {
            style.textAlign = 'center';
            style.display = 'block';
        }
        return style;
    }

    render() {
        return (
            <div style={this.setCenter()} className="loading">
                <img style={this.setStyleImg()} src={loading} alt="Loading..."/>
            </div>
        );
    }
}

export default Loading;
