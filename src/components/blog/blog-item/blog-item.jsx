import React, { Component } from 'react';
import './blog-item.sass';
import { Link } from 'react-router-dom';
import Loading from '../../loading/loading';
import Modal from '../../modal/modal';

class BlogItem extends Component {
    state = {
        comments: [],
        user: {},
        loading: true,
        showModal: false
    }

    componentWillMount = async () => {
        try {
            const responseUser = await fetch(`https://jsonplaceholder.typicode.com/users/${this.props.post.userId}`);
            const user = await responseUser.json();
            const responseComments = await fetch(`https://jsonplaceholder.typicode.com/posts/${this.props.post.id}/comments`);
            const comments = await responseComments.json();

            const localComments = JSON.parse(localStorage.getItem('comments')) || [];
            const filteredComments = localComments.filter(comment => comment.postId == this.props.post.id).reverse();
            comments.unshift(...filteredComments);

            console.log(comments)

            this.setState({
                user,
                comments,
                loading: false
            })
        } catch (error) {
            console.log(error)
        }
    }

    toggleModal = (bool) => {
        this.setState({
            showModal: bool
        })
    }

    showComments = () => {
        const { comments } = this.state;

        if (comments.length) {
            return comments.slice(0,3).map(comment => {
                return (
                    <div key={comment.id} className="comment">
                        <div className="comment-content">
                            <div className="name-date">
                                <div className="name-content">
                                    <span className="name">{comment.name}</span>
                                </div>
                                <span className="name-user">{comment.email}</span>
                            </div>
                            <div className="comment-desc">
                                {comment.body}
                            </div>
                        </div>
                    </div>
                );
            })
        }
        return null;

    }

    render() {
        const { post } = this.props;
        const { user, comments} = this.state;
        return (
            <div className="post">
                <div className="row">
                    <div className="col-md-2">
                        <div className="left-info">
                            <div className="comments">
                                <span onClick={() => this.toggleModal(true)} className="number-comment">{!this.state.loading ? comments.length : <Loading size='small'/>}</span>comments
                            </div>
                        </div>
                    </div>
                    <div className="col-md-10 line-dashed">
                        <div className="circle"></div>
                        <div className="right-blog">
                            <div className="posted">
                                <div>posted by {!this.state.loading ? user.name : <Loading size='small'/>}</div>
                            </div>
                            <div className="name-of-blog">
                                <Link to={`/post/${post.id}`}>{post.title}</Link>
                            </div>
                            <div className="description-of-blog">
                                <p>{post.body}</p>
                            </div>
                            <div className="continue-reading">
                                <Link to={`/post/${post.id}`} className="btn-read-more">continue reading</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal show={this.state.showModal} handleClose={() => this.toggleModal(false)}>
                    <h2>Last comments</h2>
                    {this.showComments()}
                    ...
                </Modal>
            </div>
        );
    }
}

export default BlogItem;
