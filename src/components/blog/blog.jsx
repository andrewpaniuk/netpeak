import React, { Component } from 'react';
import './blog.sass';

import Loading from '../loading/loading';
import BlogItem from './blog-item/blog-item';

import { connect } from 'react-redux';
import { NavLink, Link } from 'react-router-dom';

class Blog extends Component {

    state = {
        posts: [],
        loading: true,
        currentPage: 0,
        pageSize: 3,
        searchedPosts: []
    }


    componentWillMount = async () => {
        document.title = 'Blog';
        this.props.onSetTitle('Blog');
        try {
            const response = await fetch('https://jsonplaceholder.typicode.com/posts');
            const json = await response.json();
            this.setState({
                posts: json,
                loading: false,
                currentPage: this.props.match.params.page ? this.props.match.params.page - 1 : 0,
                size: Math.ceil(json.length / this.state.pageSize)
            })
        } catch (error) {
            console.log(error);
        }

    }

    getSearchedPosts = () => {
        const reg = new RegExp(this.props.search);
        console.log(this.state.posts.filter(post => reg.test(post.title)));
        const searchedPosts = [];
        const searchedTitle = this.state.posts.filter(post => reg.test(post.title));
        const searchedBody = this.state.posts.filter(post => reg.test(post.body));
        searchedPosts.push(...searchedBody, ...searchedTitle)
        this.setState({
            searchedPosts
        })

    }

    componentWillReceiveProps(nextProps) {
        this.getSearchedPosts();
    }
    showPagination = () => {
        return (
            <div className="blog-navigation">
                <ul className="nav">
                    <li onClick={() => this.changePage(0)}><Link activeClassName='active' to={`/page/1`}>First</Link></li>
                    {this.showPaginationItems()}
                    <li onClick={() => this.changePage(this.state.size - 1)}><Link activeClassName='active' to={`/page/${this.state.size}`}>Last</Link></li>
                </ul>
                <div>
                    {`${this.state.currentPage + 1} / ${this.state.size}`}
                </div>
            </div>
        )
    }
    showPaginationItems = () => {
        const { posts, pageSize, currentPage } = this.state;
        const size = posts.length / pageSize;
        let pagination = [];
        for (let i = 0; i <= size; i++) {
            pagination.push(i);
        }

        let startPage;
        let endPage;

        if (size <= 10) {
            startPage = 0;
            endPage = size;
        } else {
            if (currentPage <= 6) {
                startPage = 0;
                endPage = 10;
            } else if (currentPage + 4 >= size) {
                startPage = size - 9;
                endPage = size + 1;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 5;
            }
        }
        return pagination.slice(startPage, endPage).map(number => {
            return (
                <li key={number + 1} onClick={() => this.changePage(number)}><NavLink activeClassName='active' to={`/page/${number + 1}`}>{number + 1}</NavLink></li>
            )
        })
    }
    getPage = () => {
        const { posts, currentPage, pageSize } = this.state;

        return posts.slice(currentPage * pageSize, (currentPage + 1) * pageSize);
    }

    showPosts = () => {
        if (!this.state.searchedPosts.length && this.props.search == '' || this.props.search == '') {
            return this.getPage().map(post => {
                return (
                    <BlogItem key={post.id} post={post} />
                )
            })
        }
        return this.state.searchedPosts.slice(0,3).map(post => {
            return (
                <BlogItem key={post.id} post={post} />
            )
        })
    }

    changePage = (page) => {
        this.setState({
            currentPage: page
        })
    }

    render() {
        return (
            <main className="blog">
                <div className="container">
                    <div className="blog-content">
                        {/* {!this.state.loading ? this.showPosts() : <Loading center={true} />} */}
                        {!this.state.loading ? this.showPosts() : <Loading center={true} />}
                        {/* {!this.state.loading ? this.showPagination() : null} */}
                        {(!this.state.searchedPosts.length && this.props.search == '' || this.props.search == '') ? this.showPagination() : null}
                        {(!this.state.searchedPosts.length && this.props.search != '') ? 'No search result' : null}
                    </div>
                </div>
            </main>
        );
    }
}

export default connect(
    state => ({
        search: state.search
    }),
    dispatch => ({
        onSetTitle: (title) => {
            dispatch({
                type: 'SET_TITLE',
                payload: title
            })
        }
    })
)(Blog);
