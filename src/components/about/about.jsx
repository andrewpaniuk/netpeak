import React, { Component } from 'react';
import { connect } from 'react-redux';

class About extends Component {

    componentWillMount = () => {
        document.title = 'About';
        this.props.onSetTitle('About');
    }

    render() {
        return (
            <div>
                About
            </div>
        );
    }
}

export default connect(
    state => ({

    }),
    dispatch => ({
        onSetTitle: (title) => {
            dispatch({
                type: 'SET_TITLE',
                payload: title
            })
        }
    })
)(About);
