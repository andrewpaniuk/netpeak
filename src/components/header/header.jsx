import React, { Component } from 'react';
import './header.sass';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import Logo from '../logo/logo';
import Search from '../search/search';
import Navigation from '../navigation/navigation';

class Header extends Component {

    render() {
        const { title } = this.props;
        return (
            <header className="header">
                <div className="container-fluid header-nav">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-2 col-sm-5 col-xs-6">
                                <Logo/>
                            </div>
                            <div className="col-md-3">
                                <Search/>
                            </div>
                            <div className="col-md-7 width-100">
                                <Navigation/>
                            </div>
                        </div>

                    </div>

                </div>

                <div className="container-fluid main-title header-line">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="title">
                                    <span>{title}</span>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </header>


        );
    }
}

export default withRouter(connect(
    state => ({
        title: state.title
    }),
    dispatch => ({

    })
)(Header));
